#include <stdio.h>
#include <stdlib.h>

#define CHECK0 0x44
#define CHECK1 0X55

int main(int argc, char *argv[]){
	char input[100];
	int *check;
	long foo;
	int a, b, c , d;
	//allocating memory for check*
	check = (int*) malloc(2*sizeof(int));
	//adding check values to *check
	check[0] = CHECK0;
	check[1] = CHECK1;
	printf("check0's address is 0x%8x\n", &check[0]);
	printf("check1's address is 0x%8x\n", &check[1]);
	//user input
	printf("Enter an integer\n");
	scanf("%d", &foo);
	printf("Enter a String\n");
	scanf("%s",input);

	printf(input);
	printf("\n");
	
	printf("The checks were: 0x%x and 0x%x\n", CHECK0, CHECK1);
	printf("The new values are: 0x%x and 0x%x\n", check[0], check[1]);
	return 0;
	
	
}
